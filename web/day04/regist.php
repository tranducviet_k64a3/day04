<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Day03</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
</head>

<style><?php include './style.css' ?></style> 

<body>
    <?php
        $regrex = "/^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/";
        $name = $date = $gender = $department = $address = '';
        $errors = array('name' => '', 'gender' => '', 'department' => '', 'date' => '');

        if ($_SERVER["REQUEST_METHOD"] == "POST") { 
            if (empty($_POST['name'])) {
                $errors['name'] = "<h6 style='color:red;'>Hãy nhập tên</h6>";
            } else {
                $name = $_POST['name'];
            }

            if (empty($_POST['gender'])) {
                $errors['gender'] = "<h6 style='color:red;'>Hãy chọn giới tính</h6>";
            } else {
                $gender = $_POST['gender'];
            } 

            if (empty($_POST['department'])) {
                $errors['department'] = "<h6 style='color:red;'>Hãy chọn phân khoa</h6>";
            } else {
                $department = $_POST['department'];
            }

            if (empty($_POST['date'])) {
                $errors['date'] = "<h6 style='color:red;'>Hãy nhập ngày sinh</h6>";
            } elseif (!preg_match($regrex, $_POST['date'])) {
                $errors['date'] = "<h6 style='color:red;'>Hãy nhập ngày sinh đúng định dạng</h6>";
            } else {
                $date = $_POST['date'];
            }

            $address = $_POST['address'];
        }
    ?>

    <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
    <div class="center">
        <div class="container">            
            <form action="" method="POST" >
                <div class="wrapper">
                    <div>
                        <?php
                            echo $errors['name'];
                            echo $errors['gender'];
                            echo $errors['department'];
                            echo $errors['date'];
                        ?>  
                    </div>
                    <div class="label-row name">
                        <button class="compulsory">Họ và tên</button>
                        <input type="text" name="name" value="<?php echo $name ?>">
                    </div>
                    <div class="label-row gender">
                        <button class="compulsory">Giới tính</button>
                        <?php 
                            $genderArray = array("Nam", "Nữ");
                            for ($i = 0; $i < count($genderArray); $i++) {
                                echo "<div class='gender-option'>
                                    <input type='radio' name='gender' id=gender-$genderArray[$i] value=$genderArray[$i] ";
                                if ($gender == $genderArray[$i]) {
                                    echo "checked";
                                } else {
                                    echo "";
                                }
                                echo ">";
                                echo "<label>$genderArray[$i]</label>";
                                echo "</div>";
                            }
                        ?>
                    </div>
                    <div class="label-row department">
                        <button class="compulsory">Phân khoa</button>
                        <div class="select-box">
                            <input id="select-input" readonly type="text" name="department" value="<?php echo $department ?>">
                            <div class="arrow-down" id="button-dropdown"></div>
                            <ul class="dropdown hide" tabindex="-1">
                                <?php
                                    $departments = array(array("None", ""), array("MAT", "Khoa học máy tính"), array("KDL", "Khoa học vật liệu"));
                                    foreach ($departments as $department) {
                                        echo "<li value=$department[0]>$department[1]</li>";                        
                                    }
                                ?>  
                            </ul>
                        </div>
                    </div>
                    <div class="label-row date">
                        <button class="compulsory">Ngày sinh</button>
                        <div class="relative">
                            <div class="input-group date" id="datepicker">
                                <input type="text" placeholder="dd/mm/yyyy" class="form-control" name="date" value="<?php echo $date ?>">
                                <span class="input-group-append">
                                    <span class="input-group-text bg-white">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="label-row address">
                        <button>Địa chỉ</button>
                        <input type="text" name="address" value="<?php echo $address ?>">
                    </div>
                    <div class="register">
                        <button type="submit">Đăng ký</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</body>
<script>
    document.addEventListener("DOMContentLoaded", () => {
        /**
         * Đóng mở dropdown bằng button
         * CreatedBy: Viet2707 - 23/09/2022
         */
        var dropdown = document.querySelector(".dropdown")
        document.querySelector("#button-dropdown").addEventListener("click", () => {
            dropdown.classList.toggle("hide")
            dropdown.focus()
        })

        /**
         * Chọn option
         * CreatedBy: Viet2707 - 23/09/2022
         */
        var options = document.querySelectorAll(".dropdown li")
        var input = document.querySelector("#select-input")
        options.forEach(option => {
            option.addEventListener("click", () => {
                var value = option.innerHTML;
                input.setAttribute("value", value)
                input.setAttribute("option", option.getAttribute("value"))
                dropdown.classList.add("hide")
            })
        })

    });
</script>
<script type="text/javascript">
    $(function() {
        $('#datepicker').datepicker({
                format: "dd/mm/yyyy"
        });
    });
</script>
</html>